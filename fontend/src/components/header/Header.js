import React from "react";
import "../../assets/css/StylePage.css"
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";

export default function Header(){
    return(
        <header>
            <nav className="navbar navbar-default navbar-fixed-top navbar-expand-lg">
                <div className="container-fluid ">
                  <Link to="/">
                    <div className="button-header ">
                        <button type="button " className="this-header text-white header-button-blue btn-outline-success">
                            Home
                        </button>
                    </div>
                  </Link>
                    <div className="header-right">
                        <a className="nav-link active " href="#our-service">Our Service</a>
                        <a className="nav-link " href="#why-us">Why Us</a>
                        <a className="nav-link " href="#testinomial">Testinomial</a>
                        <a className="nav-link " href="#faq">FAQ</a>
                        <Link to= "/register">
                            <button className=" button-green btn btn-success " >Register</button>
                        </Link>
                    </div>
                </div>
            </nav>
        </header>
    )
}
