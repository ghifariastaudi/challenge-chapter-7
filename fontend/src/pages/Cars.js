import React from "react";
import Header from "../components/header/Header";
import ContentMainNoButton from "../components/PageContent/PageContentNoButton";
import Footer from "../components/footer/Footer";
import Filter from "../components/filter/filter";
const Cars = ()=>{
    return(
        <div>
            <Header />
            <ContentMainNoButton />
            <Filter />
            <Footer />
        </div>
    )
}

export default Cars;