import React from 'react';
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./assets/css/responsive.css"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Main from "./pages/Main";
import Cars from "./pages/Cars";
import { Provider } from "react-redux";
import store from "./redux/store";
import Register from './components/register/register';
import Login from './components/login/login';
import Dashboard from './components/dashboard';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
      <BrowserRouter>
      <Routes>
        <Route path="/" element ={<Main />}/>
        <Route path="/register" element ={<Register />} />
        <Route path="/dashboard" element={<Dashboard/>}/>
        <Route path="/login" element={<Login/>}/>
        <Route path="/Cars" element ={<Cars />}/>
      </Routes>
    </BrowserRouter>
    </Provider>

    );
  }
}
export default App;
